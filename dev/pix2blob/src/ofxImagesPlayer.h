#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxThreadedImageLoader.h"


// send host (aka ip address)

class ofxImagesPlayer{

	public:
		ofxGuiGroup gui;

		void setup();
		void update();
		void draw();
		void loadDir(string folder);
		void resize(int width, int height);
		ofParameter<bool> enable;

		void exit();

	    int canvasWidth;
	    int canvasHeight;

		ofFbo canvasFbo;

		vector<ofImage> images;
    	ofxThreadedImageLoader loader;
    	vector<string> imagePath;

	    string folderName;
   		
		int frameTotal;

		string playHeadNames[3];
    

    	bool nextFrame = 0;
    	bool prevFrame = 0;
    	int lastDirection = 0;
    	int frameNow = 0;
    	int playHead =0 ;
	    int loadHead = 0;



		ofxButton btnNext;
	    ofxButton btnPrev;

		void set_playhead(int frame);
		ofParameter <int> set_playhead_int;
		int set_playhead_int_old;
		void set_playhead_listen(int &frame);


		void next();
		void prev();

		bool isFolderLoaded =0 ;
		ofParameter<string> imagesFolder;
		ofParameter<string> oldImagesFolder;

		ofParameter<bool> playback;
		ofParameter <int> interval;
		long unsigned previousMillis=0;
		//long unsigned interval;
};
