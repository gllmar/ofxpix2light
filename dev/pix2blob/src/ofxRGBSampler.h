#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxGui.h"
#include "ofxArtnet.h"

#define HOST "localhost"
#define PORT 12345

// send host (aka ip address)

class ofxRGBSampler{

	public:

		void setup();
		void update(ofTexture texture);
		void draw();
		void draw(int x, int y, float scale);

		ofxGuiGroup gui;

		ofFbo fboInput;
		ofFbo fboData;
		ofParameter <int> offset_x;
		ofParameter <int> offset_y;
		ofParameter <int> sampler_width;
		ofParameter <int> sampler_height;
		ofParameter <int> data_x;
		ofParameter <int> data_y;
		ofParameter <bool> printOnceRGBA;
		ofParameter <bool> printOnceRGB;
		ofParameter <bool> sendOSC_RGB;
		ofParameter <bool> send_ARTNET;
		ofParameter <bool> draw_box;
		int sampler_width_old=0;
		int sampler_height_old=0;
		int data_x_old=0;
		int data_y_old=0;	

		ofPixels pixData;
		ofBuffer bufData;
		void resize();	
		ofxOscSender sender;
		ofxArtnetSender artnet;



};
