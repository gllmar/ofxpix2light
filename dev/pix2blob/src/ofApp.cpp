#include "ofApp.h"


//--------------------------------------------------------------
void ofApp::setup(){
	ofSetWindowTitle("pix2blob");
	ofSetFrameRate(60); // run at 60 fps
	ofSetVerticalSync(true);
	RGBSampler.setup();
	gui.setup("gui");
	gui.add(&RGBSampler.gui);
	texture_mix.allocate(512,512,GL_RGBA);
	imagesPlayer.setup();
	gui.add(&imagesPlayer.gui);
	videoGrabber.setup();
	gui.add(&videoGrabber.gui);
	gui.loadFromFile("settings.xml");

}

//--------------------------------------------------------------
void ofApp::update(){
	imagesPlayer.update();
	videoGrabber.update();

	texture_mix.begin();
//	ofBackgroundGradient(0, 255);

	imagesPlayer.draw();
	videoGrabber.draw();
	texture_mix.end();

	RGBSampler.update(texture_mix.getTexture());



}

//--------------------------------------------------------------
void ofApp::draw(){
	
	
	texture_mix.draw(200,200);
	RGBSampler.draw();
	
	RGBSampler.draw(200,200,1);

	gui.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if(key == OF_KEY_LEFT) 
	{
		imagesPlayer.prev();
	}	
    if(key == OF_KEY_RIGHT) 
	{
		imagesPlayer.next();
	}	

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo)
{
if( dragInfo.files.size() > 0 ){
		dragPt = dragInfo.position;
		imagesPlayer.playback=0;
		
		for(unsigned int k = 0; k < dragInfo.files.size(); k++){
			cout<<(dragInfo.files[k])<<endl;
			imagesPlayer.loadDir(dragInfo.files[k]);
		}
	}
}
