#pragma once

#include "ofMain.h"
#include "ofxGui.h"

class ofxVideoGrabber
{

	public:
		ofxGuiGroup gui;

		void setup();
		void update();
		void draw();
		void resize(int width, int height);

		void populate();
		ofParameter <bool> enable;

		ofVideoGrabber vidGrabber;
		ofFbo  fbo;
		int camWidth = 640;
		int camHeight = 360;
		
		vector<ofVideoDevice> devices;
		void select(int deviceID);
		void selectDevice(int &dev);

		ofParameter <int> device_selection; 
		
		void toggleEnable(bool &b_enable);

};
