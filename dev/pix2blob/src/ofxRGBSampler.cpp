#include "ofxRGBSampler.h"

//--------------------------------------------------------------
void ofxRGBSampler::setup()
{
	ofDisableArbTex();
	gui.setName("RGBSampler");
	gui.add(offset_x.set("offset_x", 0 ,0, 512));
	gui.add(offset_y.set("offset_y", 0 ,0, 512));
	gui.add(sampler_width.set("sampler_width", 512,1,512));
	gui.add(sampler_height.set("sampler_height", 1, 1, 512));
	gui.add(data_x.set("data_x", 170,1, 170));
	gui.add(data_y.set("data_y",1, 1,170));
	gui.add(printOnceRGBA.set("print_once_RGBA",0));
	gui.add(printOnceRGB.set("print_once_RGB",0));
	gui.add(sendOSC_RGB.set("sendOSC_RGB",0));
	gui.add(send_ARTNET.set("send_artnet",0));
	gui.add(draw_box.set("draw_box",1));
	artnet.setup("127.0.0.1");
	//artnet.enableThread(40.0);

	sender.setup(HOST, PORT);
	resize();

}

//--------------------------------------------------------------
void ofxRGBSampler::update(ofTexture texture)
{
	if(sampler_width_old!=sampler_width 
		|| sampler_height_old!=sampler_height 
		|| data_x_old !=data_x 
		|| data_y_old != data_y)
	{	
		resize();
	}

	fboInput.begin();
		texture.setTextureWrap(GL_REPEAT, GL_REPEAT);
		texture.drawSubsection(0,0,sampler_width,sampler_height,offset_x,offset_y,sampler_width,sampler_height);
	fboInput.end();
	
	fboData.begin();
		fboInput.draw(0,0,data_x,data_y);
	fboData.end();

	fboData.readToPixels(pixData,0);

	if(sendOSC_RGB)
	{
		ofPixels RGB = pixData;
		RGB.setNumChannels(3);
		bufData.reserve(data_x*data_y*3);
		bufData.set(ofToString(RGB.getData()));
		bufData.resize(data_x*data_y*3);
		ofxOscMessage m;
		m.setAddress("/rgb");
		m.addBlobArg(bufData);
		sender.sendMessage(m);
	}

	if(send_ARTNET)
	{
		ofPixels RGB;
		fboData.readToPixels(RGB);
		RGB.setNumChannels(3);
		ofxArtnetMessage m(RGB);
		m.setUniverse(0, 0, 0);
		artnet.sendArtnet(m);
	}

	if(printOnceRGBA)
	{
		printOnceRGBA=0;
		bufData.set(ofToString(pixData.getData()));
		ofxOscMessage m;
		m.setAddress("/rgba");
		m.addBlobArg(bufData);
		sender.sendMessage(m);
		ofLog() << "sending image with size: " << bufData.size();
	}
	if(printOnceRGB)
	{
		printOnceRGB=0;
		ofPixels RGB = pixData;
		RGB.setNumChannels(3);
		bufData.set(ofToString(RGB.getData()));
		ofxOscMessage m;
		m.setAddress("/rgb");
		m.addBlobArg(bufData);
		sender.sendMessage(m);
		ofLog() << "sending image with size: " << bufData.size();
	}


}

//--------------------------------------------------------------
void ofxRGBSampler::draw()
{
	fboData.draw(220,10,data_x,data_y);
}
//--------------------------------------------------------------

void ofxRGBSampler::draw(int x, int y, float scale)
{
	if (draw_box)
	{
		ofPushStyle();
		ofSetHexColor(0x0cb0b6);
		ofFill();
		ofDrawRectangle(x + offset_x -1 , y + offset_y -1 , sampler_width +2  , sampler_height+2);
		ofSetHexColor(0xffff00);
		ofDrawRectangle(x + offset_x -1 , y + offset_y -1 , data_x +2  , data_y+2);
		ofPopStyle();
	}
		
		//fboData.draw(x+offset_x,y+offset_y,data_x,data_y);
		fboData.draw(x+offset_x,y+offset_y,sampler_width,sampler_height);


}


//--------------------------------------------------------------
void ofxRGBSampler::resize()
{
	bufData.allocate(data_x*4*data_y*4);
	fboData.allocate(data_x,data_y, GL_RGBA);
	fboInput.allocate(sampler_width,sampler_height, GL_RGBA);
	pixData.allocate(data_x, data_y, OF_IMAGE_COLOR_ALPHA);
	sampler_width_old=sampler_width;
	sampler_height_old=sampler_height;
	data_x_old=data_x;
	data_y_old=data_y;
}

//--------------------------------------------------------------