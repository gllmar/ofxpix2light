#include "ofxVideoGrabber.h"

//--------------------------------------------------------------
void ofxVideoGrabber::setup()
{
    gui.setup("videoGrabber");
    gui.add(enable.set("enable",0));
    gui.add(device_selection.set("device",0,0,1));
    populate();
    device_selection.addListener(this, &ofxVideoGrabber::selectDevice);
    enable.addListener(this, &ofxVideoGrabber::toggleEnable);

}

void ofxVideoGrabber::toggleEnable(bool &b_enable)
{
    if(!enable)
    {
    } else {
    }
}

void ofxVideoGrabber::populate()
{
    devices = vidGrabber.listDevices();

    for(size_t i = 0; i < devices.size(); i++){
        if(devices[i].bAvailable){
            //log the device
            ofLogNotice() << devices[i].id << ": " << devices[i].deviceName;
        }else{
            //log the device and note it as unavailable
            ofLogNotice() << devices[i].id << ": " << devices[i].deviceName << " - unavailable ";
        }
    }
    device_selection.setMax(devices.size());
}

void ofxVideoGrabber::select(int deviceID)
{
    vidGrabber.close();
    if(devices[deviceID].bAvailable)
    {
        vidGrabber.setDeviceID(deviceID);
        vidGrabber.setDesiredFrameRate(60);
        vidGrabber.initGrabber(camWidth, camHeight);
    }
}

void ofxVideoGrabber::selectDevice(int &dev)
{
    select(device_selection);
}
//--------------------------------------------------------------
void ofxVideoGrabber::resize(int width, int height)
{

}


//--------------------------------------------------------------
void ofxVideoGrabber::update()
{
    if(enable)
    {
        vidGrabber.update();
    }
}

//--------------------------------------------------------------
void ofxVideoGrabber::draw()
{
    if(enable)
    {
        vidGrabber.draw(0, 0);
    }
}
//--------------------------------------------------------------
