#include "ofxImagesPlayer.h"

//--------------------------------------------------------------
void ofxImagesPlayer::setup()
{
	canvasWidth = 1280;
    canvasHeight = 720;
    
    gui.setup("imagesPlayer");
    gui.add(enable.set("enable",1));
    gui.add(set_playhead_int.set("playhead",0,0,100));
    gui.add(btnNext.setup("next"));
    gui.add(btnPrev.setup("prev"));
    gui.add(imagesFolder.set("imagesFolder","image"));
    gui.add(playback.set("playback",1));
    gui.add(interval.set("interval",100, 16, 1000));

    images.resize(3);


    resize(canvasWidth,canvasHeight);

    set_playhead_int.addListener(this, &ofxImagesPlayer::set_playhead_listen);
    btnNext.addListener(this, &ofxImagesPlayer::next);
    btnPrev.addListener(this, &ofxImagesPlayer::prev);

}

//--------------------------------------------------------------
void ofxImagesPlayer::resize(int width, int height)
{
	canvasWidth = width;
    canvasHeight = height;
    canvasFbo.allocate(canvasWidth, canvasHeight, GL_RGBA);

}


//--------------------------------------------------------------
void ofxImagesPlayer::update()
{
    if (enable)
    {
 if (imagesFolder.get() != oldImagesFolder.get())
    {
        loadDir(imagesFolder);
    }

    if (playback)
    {
        long unsigned currentMillis =ofGetElapsedTimeMillis();
        if( currentMillis - previousMillis >= interval)
        {
            next();
            previousMillis = currentMillis; //reset the previousMillis value here to currentMillis so that in the loop next time it triggers again.
        } 
    }


    if(isFolderLoaded)
    {
        canvasFbo.begin();
        images[playHead].draw(0, 0, canvasWidth , canvasHeight);
        canvasFbo.end();
    
    }

    }
   

}

//--------------------------------------------------------------
void ofxImagesPlayer::draw()
{
    if(enable)
    {
        if(isFolderLoaded)
    {
        ofPushStyle();
        canvasFbo.draw(0,0,canvasWidth,canvasHeight);
        ofPopStyle();
    }
    } 

}
//--------------------------------------------------------------

void ofxImagesPlayer::loadDir(string folder)
{    
    isFolderLoaded = 0;
    imagesFolder =folder;
    oldImagesFolder=folder;
    folderName = folder;
    string path = folder;
    ofDirectory dir(path);
    if (dir.isDirectory())
    {
        dir.allowExt("jpg");
        dir.allowExt("png");
        dir.allowExt("jpeg");
        dir.listDir();
        dir.sort();
        dir=dir.getSorted(); 
        frameTotal = dir.size();
        
        if(dir.isDirectoryEmpty(dir)==0)
        {
            isFolderLoaded = 1;
            cout<<frameTotal<<endl;
            set_playhead_int.setMax(frameTotal);
                // storer les path des images dans un vector de string
            for (int i = 0; i < dir.size(); i++)
            {
                imagePath.push_back (dir.getPath(i));
                //cout<<dir.getPath(i)<<endl;
            }   
            for (int i = 0; i < images.size(); i++)
            {
                loader.loadFromDisk(images[i], imagePath[(i+imagePath.size()-1)%imagePath.size()]);
                playHead = 0;
                playHeadNames[i] =imagePath[i];
            }
        }
    }
}

void ofxImagesPlayer::set_playhead(int frame)
{
    if(isFolderLoaded)
    {
        bool is_playing = playback;
        playback = 0;
        loader.loadFromDisk(images[0], imagePath[(frame-1)%frameTotal]);
        loader.loadFromDisk(images[1], imagePath[(frame)%frameTotal]);
        loader.loadFromDisk(images[2], imagePath[(frame+1)%frameTotal]);
        playHead = 1;
        frameNow = frame;
        set_playhead_int_old=frame; 
        set_playhead_int = frame;
        ofSleepMillis(50);
        playback=is_playing;
    }
}

void ofxImagesPlayer::set_playhead_listen(int &frame)
{
    if(set_playhead_int!=set_playhead_int_old)
    {
        set_playhead(set_playhead_int);
    }
}

void ofxImagesPlayer::next()
{
    if(isFolderLoaded)
    {
        if(lastDirection == -1) 
        {
            frameNow = (frameNow+frameTotal+3)%frameTotal;
        } else {
            frameNow = (frameNow+frameTotal+1)%frameTotal;
        }
    
        playHead = (playHead+images.size()+1)%images.size();
        loadHead = (playHead+images.size()+1)%images.size();
        loader.loadFromDisk(images[loadHead], imagePath[frameNow]);
        playHeadNames[loadHead] =imagePath[loadHead];
        lastDirection = 1;
        set_playhead_int_old=frameNow;
        set_playhead_int=frameNow;
    }
}


void ofxImagesPlayer::prev()
{
    if(isFolderLoaded)
    {
    //cout<<loader.images_async_loading<<endl;    
    
    if (lastDirection == 1 )
    {
        frameNow = (frameNow+frameTotal-3)%frameTotal;    
    } else {
        frameNow = (frameNow+frameTotal-1)%frameTotal;
    }
    
    playHead = (playHead+images.size()-1)%images.size();
    loadHead = (playHead+images.size()-1)%images.size();
    loader.loadFromDisk(images[loadHead], imagePath[frameNow]);
    playHeadNames[loadHead] =imagePath[loadHead];
    
    lastDirection = -1;
    set_playhead_int_old=frameNow;
    set_playhead_int=frameNow;
    }
}


void ofxImagesPlayer::exit (){
    
    loader.stopThread();
    cout<<"killing ofxThreadedImageLoader thread"<<endl;

}

