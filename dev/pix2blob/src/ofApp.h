#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxRGBSampler.h"
#include "ofxImagesPlayer.h"
#include "ofxVideoGrabber.h"



class ofApp : public ofBaseApp{

	public:

		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y);
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		ofxPanel gui;

		ofxRGBSampler RGBSampler;
		ofFbo texture_mix;

		ofxImagesPlayer imagesPlayer;
		glm::vec2 dragPt;

		ofxVideoGrabber videoGrabber;

};
