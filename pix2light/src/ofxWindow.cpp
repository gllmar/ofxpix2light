#include "ofxWindow.h"

ofxWindow::ofxWindow() {
    isFullscreen = false;
    isCursorVisible = true;
}

void ofxWindow::setup(bool fullscreen, bool showCursor) {
    isFullscreen = fullscreen;
    isCursorVisible = showCursor;

    ofSetFullscreen(isFullscreen);
    if (isCursorVisible) {
        ofShowCursor();
    } else {
        ofHideCursor();
    }

    // Setup the GUI elements
    gui.setup("Window Settings");
    gui.add(fullscreenToggle.setup("Fullscreen", isFullscreen));
    gui.add(fpsSlider.setup("FPS", 60, 1, 144));  // Default FPS is 60, with a range from 1 to 120
    gui.add(fpsLabel.setup("Current FPS: ", ""));


    // Add listeners to GUI elements
    fullscreenToggle.addListener(this, &ofxWindow::l_toggleFullscreen);
    fpsSlider.addListener(this, &ofxWindow::l_setFrameRate);
}

void ofxWindow::handleKeyPress(int key) {
    switch(key) {
        case OF_KEY_F2: 
            toggleCursor();
            break;
        case OF_KEY_F11: 
            toggleFullscreen();
            break;
    }
}


void ofxWindow::toggleCursor() {
    isCursorVisible = !isCursorVisible;
    if (isCursorVisible) {
        ofShowCursor();
    } else {
        ofHideCursor();
    }
}


bool ofxWindow::l_setFrameRate(int& fps) {
    ofSetFrameRate(fps);
    return true;  // Return true to indicate the event was handled
}


void ofxWindow::toggleFullscreen() {
    isFullscreen=!isFullscreen;
    ofSetFullscreen(isFullscreen);
}

bool ofxWindow::l_toggleFullscreen(bool& isFull) {
    isFullscreen = isFull;
    ofSetFullscreen(isFullscreen);
    return true;  // Return true to indicate the event was handled
}


void ofxWindow::updateFPSLabel() {
    fpsLabel = ofToString(ofGetFrameRate());
}

