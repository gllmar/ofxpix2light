#include "ofxRGBSampler.h"

//--------------------------------------------------------------
void ofxRGBSampler::setup(int index)
{
	ofDisableArbTex();
	std::string str = "RGBSampler_" + std::to_string(index);
	gui.setName(str);
	gui.add(enabled.set("enabled", 1));
	gui.add(opacity.set("opacity", 1));
	gui.add(offset_x.set("offset_x", 0 ,0, 1280));
	gui.add(offset_y.set("offset_y", 0 ,0, 720));
	gui.add(sampler_width.set("sampler_width", 512,1,512));
	gui.add(sampler_height.set("sampler_height", 1, 1, 512));
	//gui.add(data_x.set("data_x", 170,1, 170));
	//gui.add(data_y.set("data_y",1, 1,170));
	gui.add(fixture_pixel_count_x.set("fixture_pixel_count_x", 170, 1, 512)); // Default value is 170, can be changed
	gui.add(fixture_pixel_count_y.set("fixture_pixel_count_y", 1, 1, 512));   // Default value is 1, can be changed
	gui.add(subdivision_x.set("subdivision_x",1,1,10));
	gui.add(subdivision_y.set("subdivision_y",1,1,10));
	gui.add(printOnceRGBA.set("print_once_RGBA",0));
	gui.add(printOnceRGB.set("print_once_RGB",0));
	gui.add(sendOSC_RGB.set("sendOSC_RGB",0));
	gui.add(send_ARTNET.set("send_artnet",0));
	gui.add(artnet_start_universe.set("artnet_start_universe",index,0,127));
	gui.add(draw_box.set("draw_box",1));
	artnet.setup("127.0.0.1");
	//artnet.enableThread(44.0);
	//artnet.enableThread(40.0);

	sender.setup(HOST, PORT);
	resize();

}

//--------------------------------------------------------------
void ofxRGBSampler::update(ofTexture texture)
{

	data_x = fixture_pixel_count_x * subdivision_x;
	data_y = fixture_pixel_count_y * subdivision_y;


	if(sampler_width_old != sampler_width 
		|| sampler_height_old != sampler_height 
		|| data_x_old != data_x 
		|| data_y_old != data_y
		|| subdivision_x_old != subdivision_x
		|| subdivision_y_old != subdivision_y)
	{	
		resize();
	}

if (enabled)
{
	 if (selected) {
            if (ofGetMousePressed(OF_MOUSE_BUTTON_LEFT)) {
                handleMouseDragged(ofGetMouseX(), ofGetMouseY());
            } else {
                handleMouseReleased();
            }
        }

	fboInput.begin();
		ofPushStyle();
		ofSetColor(255*opacity);
		texture.setTextureWrap(GL_REPEAT, GL_REPEAT);
		texture.drawSubsection(0,0,sampler_width,sampler_height,offset_x,offset_y,sampler_width,sampler_height);
		ofPopStyle();
	fboInput.end();
	
	// subdivision...
	fboData.begin();
		fboInput.draw(0,0,data_x,data_y);
	fboData.end();
	int i=0;
	for (int _y = 0; _y < subdivision_y; _y++) 
		{
			for (int _x =0; _x < subdivision_x; _x++){
			subfbo[i].begin();
				fboInput.getTexture().drawSubsection(0,0,data_x,data_y,_x*sampler_width/subdivision_x, _y*sampler_height/subdivision_y,sampler_width/subdivision_x, sampler_height/subdivision_y);
			subfbo[i].end();
			i++;
			}
        }

	fboData.readToPixels(pixData,0);


    if (send_ARTNET) {
        fboData.readToPixels(pixData);  // Assuming you want to read the data from fboData to pixData
        pixData.setNumChannels(3);
        sendDataAcrossUniverses(pixData, artnet_start_universe);
    }

	// if(send_ARTNET)
	// {
	// i=0;
	// int j=0;
	// 	for (int _y = 0; _y < subdivision_y; _y++) 
	// 	{
	// 		for (int _x =0; _x < subdivision_x; _x++){

	// 			universe_0.begin();
	// 				subfbo[i].draw(0,data_y*i,data_x,data_y);
	// 			universe_0.end();

	// 			i++;
	// 		}
    //     }
	// 	ofPixels RGB_0;
	// 	universe_0.readToPixels(RGB_0);
	// 	RGB_0.setNumChannels(3);
	// 	ofxArtnetMessage m(RGB_0);
	// 	m.setUniverse15(artnet_start_universe);
	// 	//m.setUniverse(artnet_start_universe_msb, artnet_start_universe_sb, artnet_start_universe_lsb); 
	// 	artnet.sendArtnet(m);
		
	// 	// ofPixels RGB_1;
	// 	// universe_1.readToPixels(RGB_1);
	// 	// RGB_1.setNumChannels(3);
	// 	// ofxArtnetMessage n(RGB_1);
	// 	// n.setUniverse15(artnet_start_universe+1);
	// 	// //m.setUniverse(artnet_start_universe_msb, artnet_start_universe_sb, artnet_start_universe_lsb); 
	// 	// artnet.sendArtnet(n);
	
	// }


	if(sendOSC_RGB)
	{
		ofPixels RGB = pixData;
		RGB.setNumChannels(3);
		bufData.reserve(data_x*data_y*3);
		bufData.set(ofToString(RGB.getData()));
		bufData.resize(data_x*data_y*3);
		ofxOscMessage m;
		m.setAddress("/rgb");
		m.addBlobArg(bufData);
		sender.sendMessage(m);
	}

	if(printOnceRGBA)
	{
		printOnceRGBA=0;
		bufData.set(ofToString(pixData.getData()));
		ofxOscMessage m;
		m.setAddress("/rgba");
		m.addBlobArg(bufData);
		sender.sendMessage(m);
		ofLog() << "sending image with size: " << bufData.size();
	}
	if(printOnceRGB)
	{
		printOnceRGB=0;
		ofPixels RGB = pixData;
		RGB.setNumChannels(3);
		bufData.set(ofToString(RGB.getData()));
		ofxOscMessage m;
		m.setAddress("/rgb");
		m.addBlobArg(bufData);
		sender.sendMessage(m);
		ofLog() << "sending image with size: " << bufData.size();
	}
}
}

//--------------------------------------------------------------
void ofxRGBSampler::draw()
{
	fboData.draw(0,0,data_x,data_y);
}
//--------------------------------------------------------------

void ofxRGBSampler::draw(int x, int y, float scale)
{
	if(enabled)
	{
		    if (ofGetMousePressed(OF_MOUSE_BUTTON_LEFT) && !selected) {
        handleMousePressed(ofGetMouseX(), ofGetMouseY());
        initialScale = sampler_width;
    }

if (draw_box)
	{
		ofPushStyle();
		ofSetHexColor(0x0cb0b6);
		ofNoFill();
		ofDrawRectangle(x + offset_x -1 , y + offset_y -1 , sampler_width +2  , sampler_height+2);
		ofSetHexColor(0xffff00);
		ofDrawRectangle(x + offset_x -1 , y + offset_y -1 , data_x +2  , data_y+2);
		ofPopStyle();
	}
		
		//fboData.draw(x+offset_x,y+offset_y,data_x,data_y);

		//fboData.draw(x+offset_x,y+offset_y,sampler_width,sampler_height);
	int i=0;
	for (int _y = 0; _y < subdivision_y; _y++) 
		{
			for (int _x =0; _x < subdivision_x; _x++){
			subfbo[i].draw(x+offset_x + _x * sampler_width/subdivision_x , y+offset_y+ _y*sampler_height/subdivision_y, sampler_width/subdivision_x, sampler_height/subdivision_y);
			i++;
			}
        }
	if(draw_box)
	{
		ofPushStyle();
		ofSetHexColor(0xFF0000);
		ofNoFill();
		for (int _y = 0; _y < subdivision_y; _y++) 
		{
			for (int _x =0; _x < subdivision_x; _x++)
			ofDrawRectangle(x+offset_x + _x * sampler_width/subdivision_x , y+offset_y+ _y*sampler_height/subdivision_y, sampler_width/subdivision_x, sampler_height/subdivision_y );
        }
		ofPopStyle();
	}
	}
	

}
//--------------------------------------------------------------


// void ofxRGBSampler::sendDataAcrossUniverses(const ofPixels& RGBData, int startUniverse) {
//     int totalData = RGBData.size();
//     int dataSent = 0;
//     int universeIndex = startUniverse;
//     ofPixels currentData;
// 	ofPixels tempData = RGBData;  // Create a non-const copy

//     while (dataSent < totalData) {
//         int dataSizeToSend = std::min(512, totalData - dataSent);
        
//         // Extract the relevant portion of the data using the non-const copy
//         currentData.cropTo(tempData, dataSent, 0, dataSizeToSend, 1);
        
//         ofxArtnetMessage m(currentData);
//         m.setUniverse15(universeIndex);
//         artnet.sendArtnet(m);

//         dataSent += dataSizeToSend;
//         universeIndex++;
//     }
// }

void ofxRGBSampler::sendDataAcrossUniverses(const ofPixels& RGBData, int startUniverse) {
    int totalData = RGBData.size();
    int dataSent = 0;
    int universeIndex = startUniverse;

    while (dataSent < totalData) {
        int dataSizeToSend = std::min(512, totalData - dataSent);
        ofPixels currentData;
		currentData.allocate(512, 1, RGBData.getNumChannels());
		currentData.set(0);
		// for (int i = 0; i < dataSizeToSend * RGBData.getNumChannels(); i++) {
        //     currentData[i] = RGBData[dataSent + i];
        // }

		for (int i = 0; i < 512; i++) {
			if(i<dataSizeToSend){
				currentData[i] = RGBData[dataSent + i];
			} else {
				currentData[i]=0;
			}
            
        }

        //currentData.setFromPixels(&RGBData.getData()[dataSent], dataSizeToSend, 1, OF_IMAGE_COLOR);
        
        ofxArtnetMessage m(currentData);
        m.setUniverse15(universeIndex);
        artnet.sendArtnet(m);

        dataSent += dataSizeToSend;
        universeIndex++;
    }
}



//--------------------------------------------------------------

void ofxRGBSampler::resize()
{
	bufData.allocate(data_x*4*data_y*4);
	fboData.allocate(data_x,data_y, GL_RGBA);
	fboInput.allocate(sampler_width,sampler_height, GL_RGBA);
	pixData.allocate(data_x, data_y, OF_IMAGE_COLOR_ALPHA);
	sampler_width_old=sampler_width;
	sampler_height_old=sampler_height;
	data_x_old=data_x;
	data_y_old=data_y;
	subdivision_x_old=subdivision_x;
	subdivision_y_old=subdivision_y;
	subfbo.resize(subdivision_x*subdivision_y);
	for (int i = 0; i < subfbo.size(); i++) {
            subfbo[i].allocate(fixture_pixel_count_x, fixture_pixel_count_y, GL_RGBA);  // Assuming you want a RGBA texture.
        }
	// universe_0.allocate(data_x,data_y*subdivision_y*subdivision_x, GL_RGBA);
	// universe_1.allocate(data_x,data_y*subdivision_y*subdivision_x, GL_RGBA);
}

//--------------------------------------------------------------
void ofxRGBSampler::handleMousePressed(int x, int y) {
    if (x >= offset_x && x <= offset_x + sampler_width &&
        y >= offset_y && y <= offset_y + sampler_height) {
        selected = true;
        dragging = true;
        initialMousePos.set(x, y);
        initialSamplerPos.set(offset_x, offset_y);
    }
}
//--------------------------------------------------------------
void ofxRGBSampler::handleMouseReleased() {
    selected = false;
    dragging = false;
    scaling = false;
}
//--------------------------------------------------------------
void ofxRGBSampler::handleMouseDragged(int x, int y) {
    if (selected) {
        if (dragging) {
            offset_x = initialSamplerPos.x + (x - initialMousePos.x);
            offset_y = initialSamplerPos.y + (y - initialMousePos.y);
        }
        if (scaling) {
            float diffX = x - initialMousePos.x;
            float diffY = y - initialMousePos.y;
            float scaleChange = (diffX + diffY) * 0.01; // Adjust the scale change rate
            sampler_width = initialScale * (1 + scaleChange);
            sampler_height = initialScale * (1 + scaleChange);
        }
    }
}