#pragma once
#include "ofxGui.h"
#include "ofMain.h"

class ofxWindow {
public:
    ofxWindow(); 

    void setup(bool fullscreen, bool showCursor);
    void handleKeyPress(int key);
    void toggleFullscreen();
    void setFrameRate(int fps);
    void toggleCursor();
    bool l_toggleFullscreen(bool& isFull);  // Modified return type and parameter
    bool l_setFrameRate(int& fps); 
    ofxPanel& getWindowGui();  // Getter for the GUI panel
    ofxGuiGroup gui;  // This is the main GUI group
    void updateFPSLabel();

private:
    bool isFullscreen;
    bool isCursorVisible;
    

    // GUI elements
    ofxToggle fullscreenToggle;
    ofxIntSlider fpsSlider;
    ofxLabel fpsLabel;  // FPS label
};
