#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxGui.h"
#include "ofxArtnet.h"

#define HOST "localhost"
#define PORT 12345

class ofxRGBSampler {
public:
    void setup(int index);
    void update(ofTexture texture);
    void draw();
    void draw(int x, int y, float scale);
    void sendDataAcrossUniverses(const ofPixels& RGBData, int startUniverse); // Added function

    ofxGuiGroup gui;
    ofParameter<bool> enabled;
    ofParameter<float> opacity;
    ofParameter<int> offset_x;
    ofParameter<int> offset_y;
    ofParameter<int> sampler_width;
    ofParameter<int> sampler_height;
	ofParameter<int> fixture_pixel_count_x;
	ofParameter<int> fixture_pixel_count_y;
    ofParameter<int> data_x;
    ofParameter<int> data_y;
    ofParameter<int> subdivision_x;
    ofParameter<int> subdivision_y;
    ofParameter<int> artnet_start_universe; // User-modifiable starting universe
    ofParameter<bool> printOnceRGBA;
    ofParameter<bool> printOnceRGB;
    ofParameter<bool> sendOSC_RGB;
    ofParameter<bool> send_ARTNET;
    ofParameter<bool> draw_box;

    int sampler_width_old=0;
    int sampler_height_old=0;
    int data_x_old=0;
    int data_y_old=0;    
    int subdivision_x_old=0;
    int subdivision_y_old=0;

private:
    ofxOscSender sender;
    ofxArtnetSender artnet;

    ofFbo fboInput;
    ofFbo fboData;
    ofPixels pixData;
    ofBuffer bufData;
    std::vector<ofFbo> subfbo;
    std::vector<ofFbo> universes; // Dynamically allocated universes
	ofFbo mainfbo;

    bool selected;
    bool dragging;
    bool scaling;
    ofVec2f initialMousePos;
    ofVec2f initialSamplerPos;
    float initialScale;

    void resize();
    void handleMousePressed(int x, int y);
    void handleMouseReleased();
    void handleMouseDragged(int x, int y);
};
