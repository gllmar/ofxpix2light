#include "ofApp.h"


//--------------------------------------------------------------
void ofApp::setup(){
	ofSetWindowTitle("pix2light");
	ofSetFrameRate(60); // run at 60 fps
	ofSetVerticalSync(true);
	window.setup(1,1);
	srcNDI.setup("srcNdi");
	srcMov.setup("SrcMov_1");
	

	gui_src.setup("gui_src", "gui_src.xml");
	gui_src.add(&window.gui);
	bDrawToScreenSize.set("Draw to Screen Size", false); // default is false
	gui_src.add(bDrawToScreenSize);
	gui_src.add(&srcNDI.gui);
	gui_src.add(&srcMov.gui);



	gui_sampler.setup("gui_sampler", "gui_sampler.xml");
	RGBSampler_1.setup(0);
	RGBSampler_2.setup(1);
	gui_sampler.add(&RGBSampler_1.gui);
	gui_sampler.add(&RGBSampler_2.gui);
	gui_sampler.loadFromFile("gui_sampler.xml");
	gui_sampler.setPosition(1280-210, 10); // Set the panel's top-left corner at (100, 100)
	

	// function to allocate size for texture_mix.allocate with a width and height define as parameter in the gui_src_src
	texture_mix.allocate(1280,720,GL_RGBA);
	gui_src.loadFromFile("gui_src.xml");
	

}

//--------------------------------------------------------------
void ofApp::update(){
	srcNDI.update();
	srcMov.update();
	window.updateFPSLabel();  // Assuming you add this method to ofxWindow
	texture_mix.begin();
		ofBackground(0); 
	//ofEnableBlendMode(OF_BLENDMODE_ALPHA);
	//ofBackgroundGradient(0, 255);
		ofEnableBlendMode(OF_BLENDMODE_ADD);

    if(bDrawToScreenSize) {
        srcNDI.draw(0, 0, ofGetWidth(), ofGetHeight());
        srcMov.draw(0, 0, ofGetWidth(), ofGetHeight());
    } else {
        srcNDI.draw(0, 0);
        srcMov.draw(0, 0);
    }



	texture_mix.end();
	
	RGBSampler_1.update(texture_mix.getTexture());
	RGBSampler_2.update(texture_mix.getTexture());



}

//--------------------------------------------------------------
void ofApp::draw(){
	
	 texture_mix.draw(0, 0);
	    if(bDrawToScreenSize) {
        texture_mix.draw(0, 0, ofGetWidth(), ofGetHeight());
    } else {
        texture_mix.draw(0, 0);
    }
	//RGBSampler.draw();
	
	RGBSampler_1.draw(0,0,1);
	RGBSampler_2.draw(0,0,1);

    if(bDrawGui) {
        gui_src.draw();
        gui_sampler.draw();
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	window.handleKeyPress(key);
	    if(key == OF_KEY_F1 ) {
        bDrawGui = !bDrawGui;
    }

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    gui_sampler.setPosition(ofGetWidth() - gui_sampler.getWidth() - 20, 10);
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo)
{
	srcMov.dragdrop(dragInfo);
}
