#pragma once

#define SAMPLER_NUM 2

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxRGBSampler.h"
#include "ofxSrcNDI.h"
#include "ofxSrcMov.h"
#include "ofxWindow.h"

class ofApp : public ofBaseApp{

	public:

		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y);
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		ofxWindow window;
		ofxPanel gui_src;
		ofFbo texture_mix;
		ofxSrcNDI srcNDI;
		ofxSrcMovFolder srcMov;

		ofxPanel gui_sampler;
		ofxRGBSampler RGBSampler_1;
		ofxRGBSampler RGBSampler_2;

		ofParameter<bool> bDrawToScreenSize;

		bool bDrawGui = true;
		

		//ofxRGBSampler [SAMPLER_NUM];
};
