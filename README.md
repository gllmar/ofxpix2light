# ofxPix2Light

## Build

Clone recursivily inside openFrameworks/apps

## submodule

```
git clone https://gitlab.com/gllmar/ofxpix2light --recursive
```
If already cloned; to update the submodules
```
git submodule update --init --recursive
```



### Sources

* blendmode: add

### FX (in source)

* brightness
* contrast
* saturation

#### Movie File

* drag and drop movie file
* loop by default
* playback rate [todo]
* prores, h264...
* save file path, load on boot


#### Video Capture device [todo]

* refresh
* dropdown to select
* try to restore on boot
* try to reload if fail


#### NDI Stream

* ofxNdi 
    * https://github.com/leadedge/ofxNDI

#### Feedback? [todo]



### Sampler

* gui to select which sampler is selected
* button to save all samplers values

#### Destination [localhost]

#### Artnet

* ofxartnet
    * 
* toggle localhost


#### SACN [todo]

#### OSC? [todo]

```mermaid
graph TD
ofxPix2light -->
openFrameworks 


```